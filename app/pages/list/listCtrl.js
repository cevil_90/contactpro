angular.module('additude')
    .controller('listCtrl', function ($q, $scope, $state, fetchListService, userService) {
        const vm = this;

        var date = (new Date()).getFullYear();
        var users;
        var filterRules = [
            { type: 'filterAge', activeRules: {from: 19, to: 65}}
        ];

        vm.filterMenu = true; // Bool deciding if filter-menu is visible or not
        vm.quickSearchBool = true; // Boolean for deciding if quick-search for mobiles are visible or not
        vm.export = []; // Collects entities to be exported
        vm.dataState = 0; // boolean for deciding if loading-state should be shown or not

        // Variables used for sorting the table
        vm.sortType     = 'name';
        vm.sortReverse  = false;
        vm.searchTable   = '';

        // Array for available genders
        vm.availableGenders = [
            {icon: 'mars', gender: 'male', state: false},
            {icon: 'venus', gender: 'female', state: false}
        ];

        // Settings for age slider
        vm.ageSort = {
            options: {
                floor: 18,
                ceil: 80
            },
            min: 18,
            max: 80
        };

        /**
         * Calculates a year backwards from to day given an int
         * @param {number} year
         * @returns {number}
         */
        var yearCalc = function (year) {
            return date - year;
        };

        // Currently applied filter rules
        var filterRules = {
            age: {from: yearCalc(vm.ageSort.options.ceil), to: yearCalc(vm.ageSort.options.floor)}
        };

        vm.goToSpecificUser = function (user) {
            userService.setTargetUser(user);
            $state.go('navbar.profile');
        };

        /**
         * Function for showing or hiding the filter menu on smaller devices
         */
        vm.toggleFilter = function () {
            vm.filterMenu = !vm.filterMenu;
            console.log(vm.filterMenu)
        };

        /**
         * Toggle quick-search for showing or hiding the filter menu on mobile devices
         */
        vm.toggleSearchFilter = function () {
            if (vm.filterMenu === false) return;
            var logo = document.getElementsByClassName("wrapper__navbar__logo"),
                quickSearchField = document.getElementsByClassName("mobile-quick-search__input");

            console.log(quickSearchField)

            vm.quickSearchBool = !vm.quickSearchBool;

            vm.quickSearchBool ? vm.searchTable = '' : angular.element(quickSearchField).focus();
            vm.quickSearchBool ? angular.element(logo).removeClass('nav-logo--hide') : angular.element(logo).addClass('nav-logo--hide');
            console.log(logo);
        };

        /**
         * Adds user to be exported in an array.
         * ( Naturally, this function and all functionality regarding the export
         * would loog very different in a live application with an API which would
         * support ID's and so on.)
         * @param {number} index
         */
        vm.addToExport = function (index) {
            var res = false;
            for (var i = 0; i < vm.export.length; i++){
                if (vm.export[i] === index) {
                    res = true;
                    vm.export.splice(i, 1);
                }
            }

            if (res !== true){ vm.export.push(index) }
        };

        /**
         * Filters users based on gender.
         * @param {number} index
         */
        vm.genderFilterClicked = function (index) {
            if (vm.availableGenders[index].state === false) {
                vm.availableGenders[0].state = false;
                vm.availableGenders[1].state = false;
                vm.availableGenders[index].state = !vm.availableGenders[index].state;

                filterGender(vm.availableGenders[index].gender)
            } else  {
                vm.availableGenders[0].state = false;
                vm.availableGenders[1].state = false;
                getUsers();
            }
        };

        /**
         * Sorts the table data based on
         * @param sortType
         */
        vm.sortTable = function (sortType) {
            vm.sortType = sortType;
            vm.sortReverse = !vm.sortReverse;
        };

        /**
         * Sets necessary variables when data is recieved and
         * changes the state for the loading-screen
         * @param {object} data
         */
        var dataRecieved = function(data){
            users = data;
            vm.tableData = data;
            vm.dataState = 1;
        };

        /**
         * filter through all the users age based on the slider
         * @returns {Promise}
         */
        var filterAge = function () {
           var deferred = $q.defer();

            setTimeout(function() {

                var yearFrom = yearCalc(vm.ageSort.max),
                    yearTo = yearCalc(vm.ageSort.min),

                    // If filter is narrowing, loop through only the currently displayed users, else, loop through all users
                    dataToProcess = yearFrom >= filterRules.age.from && yearTo <= filterRules.age.to ? vm.tableData : users,
                    filteredData = dataToProcess.filter(function(user){
                        return user.dob.substring(0,4) >= yearFrom && user.dob.substring(0,4) <= yearTo;
                    });

                // Updates the currently applied age filter rules
                filterRules.age.to = yearCalc(vm.ageSort.min);
                filterRules.age.from = yearCalc(vm.ageSort.max);

                deferred.resolve(filteredData);
                vm.dataState = 1;
            }, 1);

            return deferred.promise;
        };

        /**
         * Listens to changes made to the slider
         */
        $scope.$on("slideEnded", function() {
            vm.dataState = 0;
            filterAge().then(function(filteredData) {
                vm.tableData = filteredData;
            });
        });

        /**
         * Asks service for data based on gender
         */
        var filterGender = function (gender) {
            vm.dataState = 0;
            fetchListService.getSpecifiqGender(gender)
                .then(function (users) {
                    dataRecieved(users.results);
            });
        };

        /**
         * Asks service for all users from backend
         */
        var getUsers = function(){
            fetchListService.getUsers()
                .then(function(users) {
                    dataRecieved(users.results);
            });
        };

        /**
         * Asks for JSON from service based on selected users nad downloads file
         */
        vm.exportContacts = function () {
            if(vm.export.length < 1) return;
            fetchListService.exportContacts('json', vm.export.length)
                .then(function (fileContent) {
                    if(typeof fileContent == 'undefined' || fileContent == null || fileContent == "")
                        return ;

                    var link = document.createElement("a");
                    link.download = "info.json";
                    var data = "text/json;charset=utf-8," + encodeURIComponent(fileContent);
                    link.href = "data:" + data;
                    link.click();
                });
        };

        getUsers();
    });

