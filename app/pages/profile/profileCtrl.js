angular.module('additude')
    .controller('profileCtrl', function ($state, fetchListService, userService) {
        const vm = this;

        /**
         * Fetches the clicked user from a services since ID is not supported in the API
         */
        vm.getUser = function(){
            var res = userService.getTargetUser();
            res ? vm.user = res: $state.go('navbar.list');
        };

        vm.getUser();
    });
