var app = angular.module('additude',[
    'ui.router', 'ngResource', 'rzModule', 'fixed.table.header', 'ngAnimate'
]);

app.config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider){

// For any unmatched url, send to /business
$urlRouterProvider.otherwise("/navbar/list");
    $stateProvider
        .state('navbar', {
            url: "/navbar",
            abstract: true,
            templateUrl: "app/shared/navbar/navbarView.html",
            controller: 'navbarCtrl as navbar'
        })
        .state('navbar.list', {
            url: "/list",
            templateUrl: "app/pages/list/listView.html",
            controller : 'listCtrl as list'
        })
        .state('navbar.profile', {
            url: "/profile",
            templateUrl: "app/pages/profile/profileView.html",
            controller : 'profileCtrl as profile'
        })
}]);
