angular.module('additude')
    .service('userService', [
        function() {
            const vm = this;
            var targetUser = null;

            vm.setTargetUser = function (userObject) {
                targetUser = userObject;
            };

             vm.getTargetUser = function () {
                 return targetUser || false;
             };
        }]);