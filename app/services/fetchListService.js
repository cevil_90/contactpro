angular.module('additude')
    .service('fetchListService', ['$http', '$resource', function($http, $resource) {
        const baseUrl = 'https://randomuser.me/api/';
        var vm = this,
            url = baseUrl;

        /**
         * Clear the url from previous searches
         * @returns {object} self
         */
        vm.setupQuery = function(){
            url = baseUrl;
            return this;
        };

        /**
         * Adds the desired amount of users to be fetched, especially useful in case of implementation of
         * infinite scroll of paginated table
         * @param {number} amount
         * @returns {object} self
         */
        vm.amount = function(amount){
            if (vm.hasValue(amount)){
                url = url + queryPrepend() + 'results=' + amount;
            } else throw new Error(amount + ' must be a number');
            return this;
        };

        /**
         * Specifies a gender to fetched instead of both available
         * @param {string} gender
         * @returns {object} self
         */
        vm.specGender = function (gender) {
            if(vm.hasValue(gender) && gender === 'female' || gender === 'male'){
                url = url + queryPrepend() + 'gender=' + gender;
            } else throw new Error(gender + ' is not a valid gender');
            return this;
        };

        /**
         * Asks backend for specific format of result.
         * @param {string} format
         * @returns {object} self
         */
        vm.format = function (format) {
            if(vm.hasValue(format) && format === 'csv' || format === 'json'){
                url = url + queryPrepend() + 'format=' + format;
            } else throw new Error(gender + ' is not a valid format');
            return this;
        };

        /**
         * Includes only certain parts of the result
         * @returns {object} self
         */
        vm.include = function () {
            if(vm.hasValue(arguments)){
                for (var i = 0; i < arguments.length; i++) {
                    url = url + queryPrepend() + '?inc=' + arguments[i] + ','
                }
                url.slice(0, -1);
            } else throw new Error('No arguments where provided for include function');
            return this;
        };

        /**
         * Exludes undesired parts of the result that would otherwise be included
         * @returns {object} self
         */
        vm.exclude = function () {
            if(vm.hasValue(arguments)){
                for (var i = 0; i < arguments.length; i++) {
                    url = '?exl=' + arguments[i] + ','
                }
                url.slice(0, -1);
            } else throw new Error('No arguments where provided for exlude function');
            return this;
        };

        /**
         * Lets the order of the url-building be unimportant by taking care of the formatting
         * @returns {string}
         */
        var queryPrepend = function () {
            return baseUrl === url ? '?' : '&';
        };

        /**
         * Executes the query
         * @returns {*|Function}
         */
        vm.runQuery= function() {
            return $resource(url, {
                'get': { method: 'GET'}
            }).get().$promise;
        };

        /**
         * Makes sure the provided variable is valid
         * @param variable
         * @returns {boolean}
         */
        vm.hasValue = function (variable) {
            return typeof variable !== 'undefined' || variable !== null
        };

        /**
         * Fetches all the users
         */
        vm.getUsers = function(){
            vm.dataState = 0;
            return this
                .setupQuery()
                .amount(50)
                .runQuery()
        };

        /**
         * Fetches users based on specific gender
         * @param {string} gender
         */
        vm.getSpecifiqGender = function (gender) {
            vm.dataState = 0;
            return this
                .setupQuery()
                .amount(50)
                .specGender(gender)
                .runQuery();
        };

        /**
         * Fetches CSV file based on selected users
         *
         * @param {string} format
         * @param {array} usersIds
         */
        vm.exportContacts = function (format, usersIds) {
            return this
                .setupQuery()
                .amount(usersIds)
                .format(format)
                .runQuery();
        };

    }]);