'use strict';

var gulp = require('gulp'),
	sass = require('gulp-sass'),
	watch = require('gulp-watch'),
    concat = require('gulp-concat'),
    bytediff = require('gulp-bytediff'),
    uglify = require('gulp-uglify'),
    concatCSS = require('gulp-concat-css'),
    cleanCSS = require('gulp-clean-css'),
    jshint = require('gulp-jshint'),
    sourcemaps = require('gulp-sourcemaps'),
    gutil = require('gulp-util'),
    browserify = require('gulp-browserify');

var browserSync = require('browser-sync').create();


var reload = browserSync.reload;

gulp.task('sass', function () {

  return gulp.src([
  		'app/**/*.scss',
  		'assets/**/*.scss'
  ])

    .pipe(sass.sync().on('error', sass.logError))
    .pipe(sourcemaps.init())
    .pipe(concatCSS('styles.min.css'))
    .pipe(bytediff.start())
    .pipe(cleanCSS({compatibility: 'ie8'}))
    .pipe(bytediff.stop())
    .pipe(sourcemaps.write('./'))
    .pipe(sass())
	.pipe(browserSync.stream())
    .pipe(gulp.dest('dist'));
});


function onError(err) {
    console.log(err);
    this.emit('end');
}


/** Minify and combine all js files in app/ */
gulp.task('js', function () {

    return gulp.src([
        '!app/example.env.js',
        '!app/env.js',
        'app/**/*.js'
    ])

        .pipe(sourcemaps.init())
        .pipe(concat('app.min.js', { newLine: ';' }))
        // .pipe(ngAnnotate({ add: true }))
        .pipe(bytediff.start())
        .pipe(uglify({ mangle: true }))
        .pipe(bytediff.stop())
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest('dist'))
        .pipe(browserify({ debug: true }))
        .on('error', onError);
});


/** Concatenate and minify all css files in app directory */
gulp.task('minify-css', function() {
    return gulp.src([
        'app/style.css',
        'app/animations.css',
        'app/**/*.css'
    ])
        .pipe(sourcemaps.init())
        .pipe(concatCSS('styles.min.css'))
        .pipe(bytediff.start())
        .pipe(cleanCSS({compatibility: 'ie8'}))
        .pipe(bytediff.stop())
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest('dist'));
});


gulp.task('sassLive', function() {
  return sass('app/**/*.scss')
    .pipe(gulp.dest('dist'))
    .pipe(reload({ stream:true }));
});

// watch Sass files for changes, run the Sass preprocessor with the 'sass' task and reload
gulp.task('serve', ['sass'], function() {
    browserSync.init({
        server: {
            baseDir: "",
        }
    });

	gulp.watch('app/**/*.scss', ['sass']);
	//gulp.watch('app/**/*.js', ['js']);
	gulp.watch('app/**/*.css', ['minify-css']);
	gulp.watch('assets/**/*.css', ['minify-css']);

	gulp.watch("app/*.html").on('change', browserSync.reload);
	gulp.watch("assets/*.html").on('change', browserSync.reload);
});


gulp.task('watch', function() {
    //gulp.watch('app/**/*.js', ['js']);
    gulp.watch('app/**/*.css', ['minify-css']);
  	gulp.watch('app/**/*.scss', ['sass']);
});

//gulp.task('default', ['js', 'minify-css', 'sass', 'watch']);
gulp.task('default', ['minify-css', 'sass', 'watch']);
